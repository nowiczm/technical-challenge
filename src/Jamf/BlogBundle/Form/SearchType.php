<?php

namespace Jamf\BlogBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Search form on the website
 */
class SearchType extends AbstractType
{
    public const SEARCH_FIELD_NAME = 'term';

    /**
     * Form name
     *
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'blog_search_form';
    }

    /**
     * Builder creates search form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(self::SEARCH_FIELD_NAME, TextType::class, [
                'label' => 'Search',
                'constraints' => [
                    new Assert\NotBlank()
                ],
                'attr' => [
                    'class' => 'form-control search-input search-form-field',
                    'placeholder' => 'Enter search term...'
                ]
            ])
            ->add('Submit', SubmitType::class, [
                    'attr' => [
                        'class' => 'btn j-btn--solid search-close search-form-button',
                        'type' => 'button'
                    ]
                ]
            );
    }
}
