<?php

namespace Jamf\BlogBundle\Facade;

use Jamf\BlogBundle\Model\FilterInterface;
use Jamf\BlogBundle\Model\GroupInterface;
use Jamf\BlogBundle\Model\SortInterface;

/**
 * Facade doing operation in specific order
 */
class Finder
{
    /**
     * @var GroupInterface
     */
    private $articleGroup;

    /**
     * @var SortInterface
     */
    private $articleSort;

    /**
     * @var FilterInterface
     */
    private $articleFilter;

    /**
     * Finder constructor.
     * @param GroupInterface $articleGroup
     * @param SortInterface $articleSort
     * @param FilterInterface $articleFilter
     */
    public function __construct(
        GroupInterface $articleGroup,
        SortInterface $articleSort,
        FilterInterface $articleFilter
    ) {
        $this->articleGroup = $articleGroup;
        $this->articleSort = $articleSort;
        $this->articleFilter = $articleFilter;
    }

    /**
     * Filter, sort and group articles with custom conditions
     *
     * @param array $articles
     * @param string $sortFieldName
     * @param string $sortType
     * @param string $filterFieldName
     * @param string|int $filterfieldValue
     *
     * @return array
     */
    public function searchAndGroup(array $articles, string $sortFieldName, string $sortType, string $filterFieldName, $filterfieldValue): array
    {
        $searched = $this->articleSort->sort($articles, $sortFieldName, $sortType);
        $searched = $this->articleFilter->filter($searched, $filterFieldName, $filterfieldValue);
        $searched = $this->articleGroup->group($searched);

        return $searched;
    }
}