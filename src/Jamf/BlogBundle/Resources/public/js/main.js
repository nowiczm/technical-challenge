// Disabling search form after click search button
$(document).ready(function () {
    var searchButton = $('#blog_search_form .search-form-button');
    var searchField = $('#blog_search_form .search-form-field');

    searchButton.on('click', function () {
        if (searchField.val().length > 0) {
            $('#blog_search_form').css({
                "pointerEvents": "none",
                "opacity": 0.4
            });

            $('#blog_search_form').before('Please wait, searching articles...');
        }
    })
});