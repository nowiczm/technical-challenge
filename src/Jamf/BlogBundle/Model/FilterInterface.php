<?php

namespace Jamf\BlogBundle\Model;

/**
 * Interface using to filter array elements
 */
interface FilterInterface
{
    /**
     * @param array $dataToSort Array with data to sort
     * @param string $filterByFieldName Field name whichc be filtered
     * @param string $fieldValue Value of filed to filter
     *
     * @return array
     */
    public function filter(array $dataToSort, string $filterByFieldName, string $fieldValue): array;
}
