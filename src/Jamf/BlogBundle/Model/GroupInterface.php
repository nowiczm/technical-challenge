<?php

namespace Jamf\BlogBundle\Model;

/**
 * Interface using to group array elements
 */
interface GroupInterface
{
    /**
     * Groups array elements into new array
     *
     * @param array $dataToGroup
     *
     * @return array
     */
    public function group(array $dataToGroup): array;
}
