<?php

namespace Jamf\BlogBundle\Model;

/**
 * Entity represents the structure of an Article
 */
abstract class AbstractArticle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $entityId;

    /**
     * @var string|null
     */
    private $title;

    /**
     * @var string|null
     */
    private $language;

    /**
     * @var string|null
     */
    private $status;

    /**
     * @var string|null
     */
    private $summary;

    /**
     * @var string|null
     */
    private $metaTitle;

    /**
     * @var string|null
     */
    private $metaDescription;

    /**
     * @var string|null
     */
    private $slug;

    /**
     * @var int
     */
    private $dateCreated;

    /**
     * @var int|null
     */
    private $dateModified;

    /**
     * @var int
     */
    private $dateExpired;

    /**
     * @var int
     */
    private $createdById;

    /**
     * @var int|null
     */
    private $modifiedById;

    /**
     * @var string|null
     */
    private $content;

    /**
     * @var array
     */
    private $tags;

    /**
     * @var string|null
     */
    private $primaryCategory;

    /**
     * @var string|null
     */
    private $translatedPrimaryCategory;

    /**
     * @var string|null
     */
    private $thumbnailImage;

    /**
     * @var string|null
     */
    private $canonicalUrl;

    /**
     * @var bool
     */
    private $gated;

    /**
     * @var string|null
     */
    private $href;

    /**
     * @var null|string
     */
    private $video;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return AbstractArticle
     */
    public function setId(int $id): AbstractArticle
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }

    /**
     * @param int $entityId
     * @return AbstractArticle
     */
    public function setEntityId(int $entityId): AbstractArticle
    {
        $this->entityId = $entityId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return AbstractArticle
     */
    public function setTitle(?string $title): AbstractArticle
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return AbstractArticle
     */
    public function setLanguage(?string $language): AbstractArticle
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return AbstractArticle
     */
    public function setStatus(?string $status): AbstractArticle
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getSummary(): ?string
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     * @return AbstractArticle
     */
    public function setSummary(?string $summary): AbstractArticle
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    /**
     * @param string $metaTitle
     * @return AbstractArticle
     */
    public function setMetaTitle(?string $metaTitle): AbstractArticle
    {
        $this->metaTitle = $metaTitle;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param null|string $metaDescription
     * @return AbstractArticle
     */
    public function setMetaDescription(?string $metaDescription): AbstractArticle
    {
        $this->metaDescription = $metaDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return AbstractArticle
     */
    public function setSlug(?string $slug): AbstractArticle
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return int
     */
    public function getDateCreated(): int
    {
        return $this->dateCreated;
    }

    /**
     * @param int $dateCreated
     * @return AbstractArticle
     */
    public function setDateCreated(int $dateCreated): AbstractArticle
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDateModified(): ?int
    {
        return $this->dateModified;
    }

    /**
     * @param int|null $dateModified
     * @return AbstractArticle
     */
    public function setDateModified(?int $dateModified): AbstractArticle
    {
        $this->dateModified = $dateModified;
        return $this;
    }

    /**
     * @return int
     */
    public function getDateExpired(): int
    {
        return $this->dateExpired;
    }

    /**
     * @param int $dateExpired
     * @return AbstractArticle
     */
    public function setDateExpired(int $dateExpired): AbstractArticle
    {
        $this->dateExpired = $dateExpired;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedById(): int
    {
        return $this->createdById;
    }

    /**
     * @param int $createdById
     * @return AbstractArticle
     */
    public function setCreatedById(int $createdById): AbstractArticle
    {
        $this->createdById = $createdById;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getModifiedById(): ?int
    {
        return $this->modifiedById;
    }

    /**
     * @param int|null $modifiedById
     * @return AbstractArticle
     */
    public function setModifiedById(?int $modifiedById): AbstractArticle
    {
        $this->modifiedById = $modifiedById;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return AbstractArticle
     */
    public function setContent(?string $content): AbstractArticle
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     * @return AbstractArticle
     */
    public function setTags(array $tags): AbstractArticle
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPrimaryCategory(): ?string
    {
        return $this->primaryCategory;
    }

    /**
     * @param null|string $primaryCategory
     * @return AbstractArticle
     */
    public function setPrimaryCategory(?string $primaryCategory): AbstractArticle
    {
        $this->primaryCategory = $primaryCategory;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTranslatedPrimaryCategory(): ?string
    {
        return $this->translatedPrimaryCategory;
    }

    /**
     * @param null|string $translatedPrimaryCategory
     * @return AbstractArticle
     */
    public function setTranslatedPrimaryCategory(?string $translatedPrimaryCategory): AbstractArticle
    {
        $this->translatedPrimaryCategory = $translatedPrimaryCategory;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getThumbnailImage(): ?string
    {
        return $this->thumbnailImage;
    }

    /**
     * @param null|string $thumbnailImage
     * @return AbstractArticle
     */
    public function setThumbnailImage(?string $thumbnailImage): AbstractArticle
    {
        $this->thumbnailImage = $thumbnailImage;
        return $this;
    }

    /**
     * @return string
     */
    public function getCanonicalUrl(): ?string
    {
        return $this->canonicalUrl;
    }

    /**
     * @param string $canonicalUrl
     * @return AbstractArticle
     */
    public function setCanonicalUrl(?string $canonicalUrl): AbstractArticle
    {
        $this->canonicalUrl = $canonicalUrl;
        return $this;
    }

    /**
     * @return bool
     */
    public function getGated(): bool
    {
        return $this->gated;
    }

    /**
     * @param bool $gated
     * @return AbstractArticle
     */
    public function setGated(bool $gated): AbstractArticle
    {
        $this->gated = $gated;
        return $this;
    }

    /**
     * @return string
     */
    public function getHref(): ?string
    {
        return $this->href;
    }

    /**
     * @param string $href
     * @return AbstractArticle
     */
    public function setHref(?string $href): AbstractArticle
    {
        $this->href = $href;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getVideo(): ?string
    {
        return $this->video;
    }

    /**
     * @param null|string $video
     * @return AbstractArticle
     */
    public function setVideo(?string $video)
    {
        $this->video = $video;
        return $this;
    }
}
