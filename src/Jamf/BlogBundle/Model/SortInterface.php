<?php

namespace Jamf\BlogBundle\Model;

use Doctrine\Common\Collections\Criteria;

/**
 * Interface using to sort array elements
 */
interface SortInterface
{
    /**
     * Sorting array elements by passed criteria
     *
     * @param array $dataToSort
     * @param string $sortByFieldName
     * @param string $sortType 'ASC', 'DESC'
     *
     * @return array
     */
    public function sort(array $dataToSort, string $sortByFieldName, string $sortType = Criteria::ASC): array;
}
