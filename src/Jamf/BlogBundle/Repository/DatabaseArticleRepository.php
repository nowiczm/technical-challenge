<?php

namespace Jamf\BlogBundle\Repository;

use Jamf\BlogBundle\Exception\NoArticleException;
use Jamf\BlogBundle\Model\AbstractArticle;

/**
 * Articles from database
 */
class DatabaseArticleRepository implements ArticleRepositoryInterface
{
    /**
     * @return AbstractArticle[]
     * @throws NoArticleException
     */
    public function getList(): array
    {
        /**
         * @todo List of articles from database
         */

        return [];
    }
}