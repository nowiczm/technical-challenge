<?php

namespace Jamf\BlogBundle\Repository;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Jamf\BlogBundle\Dictionary\Api;
use Jamf\BlogBundle\Exception\NoArticleException;
use Jamf\BlogBundle\Model\AbstractArticle;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Articles from API point
 */
class ApiArticleRepository implements ArticleRepositoryInterface
{
    private const API_ARTICLE_CLASS = 'Jamf\BlogBundle\Entity\ApiArticle[]';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var string
     */
    private $apiUrl = Api::JAMF_API_PATH;

    /**
     * @param LoggerInterface $logger
     * @param ClientInterface $client
     * @param SerializerInterface $serializer
     * @param string $apiUrl
     */
    public function __construct(
        LoggerInterface $logger,
        ClientInterface $client,
        SerializerInterface $serializer,
        string $apiUrl
    ) {
        $this->logger = $logger;
        $this->client = $client;
        $this->serializer = $serializer;
        $this->apiUrl;
    }

    /**
     * @return AbstractArticle[]
     * @throws NoArticleException
     */
    public function getList(): array
    {
        $articles = [];

        try {
            $response = $this->client->request(Api::GET_METHOD, $this->apiUrl);
            if ($response->getStatusCode() === Response::HTTP_OK) {
                $apiArticles = $response->getBody();

                if (!$apiArticles->getSize()) {
                    throw new NoArticleException('No articles where found');
                }

                $articles = $this->serializer->deserialize($apiArticles, self::API_ARTICLE_CLASS, JsonEncoder::FORMAT);
            }
        } catch (RequestException|ClientException $e) {
            $this->logger->info($e->getMessage());
        }

        return $articles;
    }
}