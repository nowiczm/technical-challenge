<?php

namespace Jamf\BlogBundle\Repository;

use Jamf\BlogBundle\Exception\NoArticleException;
use Jamf\BlogBundle\Model\AbstractArticle;

/**
 * Repository of articles
 */
interface ArticleRepositoryInterface
{
    /**
     * @return AbstractArticle[]
     * @throws NoArticleException
     */
    public function getList(): array;
}
