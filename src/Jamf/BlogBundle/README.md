Back-End Web Developer Technical Challenge
-
Added modules:  

**eightpoints/guzzle-bundle**   
Client used to get data from API localized on external server.  

**symfony/serializer**  
Used to format data from API. Instead of creating i.e. hydrator class, Symfony delivers very usefull module called *Serializer*. It can be used to fill objects with data, from different data types, i.e. json, xml.

Created and tested at environment
-
PHP - 7.1.16  
Symfony - 3.4.23
 
**@todo**    
 1. Cache the search results from API point. Cachable parameter could be set to 5 minutes. Here could be used for example Redis  
 2. Add TAGS to searched fields  
 3. Need to change POST action to GET action in search form. It will allowed to be more usefull to users (search direct by url address). That kind of solution, will require add some better validator to parsed value passed by url.  
 4. Search form could be based on AJAX call or some front-end framework. We will be able to eliminate reloadinf of the page.  
 5. Create better filter classes, i.e. use strategy pattern to choose proper way of filtering   
 6. Seperate AbstractArticle class to smaller parts. Maybe by using multiple interfaces instead of one big abstract - depends from required fields in each article  
 