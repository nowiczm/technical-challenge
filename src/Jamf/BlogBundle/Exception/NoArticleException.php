<?php

namespace Jamf\BlogBundle\Exception;

use Doctrine\ORM\NoResultException;

/**
 * When no articles where found
 */
class NoArticleException extends NoResultException
{
}
