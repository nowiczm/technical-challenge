<?php

namespace Tests\Unit\Service;

use Jamf\BlogBundle\Dictionary\ArticleData;
use Jamf\BlogBundle\Entity\ApiArticle;
use Jamf\BlogBundle\Service\ApiArticleFilter;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiArticleRepositoryTest extends WebTestCase
{
    /**
     * @var ApiArticleFilter
     */
    private $apiArticleFilter;

    public function setUp()
    {
        $this->apiArticleFilter = new ApiArticleFilter();
    }

    /**
     * Return type array or empty array
     */
    public function testIsFilterReturnArray()
    {
        $filteredArticles = $this->apiArticleFilter->filter([], ArticleData::FIELD_DATE_CREATED, 123);

        $this->assertInternalType('array', $filteredArticles, 'Returned type should be set to array');
        $this->assertEquals([], $filteredArticles, 'Filter should return an empty array when no article passed');
        $this->assertEquals(0, count($filteredArticles));
    }

    /**
     * Does filter method removing appropriate elements
     */
    public function testIsFilternReturnElementsWithBiggerValue()
    {
        $articleOne = new ApiArticle();
        $articleOne->setDateCreated(1514764801);
        $articleTwo = new ApiArticle();
        $articleTwo->setDateCreated(1514764799);

        $articles = [$articleOne, $articleTwo];
        $filteredArticles = $this->apiArticleFilter->filter($articles, ArticleData::FIELD_DATE_CREATED, 1514764800);

        $this->assertEquals(1, count($filteredArticles), 'Returned array should have only one elements');
        $this->assertEquals(1514764801, $filteredArticles[0]->getDateCreated(), 'Wrong element was removed');
    }
}
