<?php

namespace Tests\Unit\Service;

use Doctrine\Common\Collections\Criteria;
use Jamf\BlogBundle\Dictionary\ArticleData;
use Jamf\BlogBundle\Entity\ApiArticle;
use Jamf\BlogBundle\Model\AbstractArticle;
use Jamf\BlogBundle\Service\ApiArticleSort;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiArticleSortTest extends WebTestCase
{
    /**
     * @var ApiArticleSort
     */
    private $apiArticleSort;

    public function setUp()
    {
        $this->apiArticleSort = new ApiArticleSort(new Criteria());
    }

    /**
     * Return type array or empty array
     */
    public function testIsSortReturnArray()
    {
        $sortedArticles = $this->apiArticleSort->sort([], ArticleData::FIELD_DATE_CREATED, 123);

        $this->assertInternalType('array', $sortedArticles, 'Returned type should be set to array');
        $this->assertEquals([], $sortedArticles, 'Sort should return an empty array when no article passed');
        $this->assertEquals(0, count($sortedArticles));
    }

    /**
     * Does sort method ordering elements in appropriate way
     */
    public function testIsSortReturnElementsWithBiggerValue()
    {
        $articleOne = new ApiArticle();
        $articleOne->setDateCreated(1514764799);
        $articleTwo = new ApiArticle();
        $articleTwo->setDateCreated(1514764800);

        $articles = [$articleOne, $articleTwo];
        $filteredArticles = $this->apiArticleSort->sort($articles, ArticleData::FIELD_DATE_CREATED, Criteria::DESC);

        /** Check is sort works good - now first element should be $articleTwo have timestamp = 1514764800 */
        /** @var AbstractArticle $article */
        foreach ($filteredArticles as $article) {
            $this->assertEquals(1514764800, $article->getDateCreated(), 'First article in array should be object with dateCreated equal 1514764800');
            break;
        }

        /** Sort function cannot remove article object from array */
        $this->assertEquals(2, count($filteredArticles), 'Returned array should have two elements');
    }
}
