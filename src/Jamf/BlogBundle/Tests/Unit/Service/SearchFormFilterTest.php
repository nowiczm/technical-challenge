<?php

namespace Tests\Unit\Service;

use Doctrine\Common\Collections\Criteria;
use Jamf\BlogBundle\Dictionary\ArticleData;
use Jamf\BlogBundle\Entity\ApiArticle;
use Jamf\BlogBundle\Model\AbstractArticle;
use Jamf\BlogBundle\Service\SearchFormFilter;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SearchFormFilterTest extends WebTestCase
{
    /**
     * @var SearchFormFilter
     */
    private $searchFormFilter;

    public function setUp()
    {
        $this->searchFormFilter = new SearchFormFilter();
    }

    /**
     * Return type array or empty array
     */
    public function testIsSearchFilterReturnArray()
    {
        $sortedArticles = $this->searchFormFilter->filter([], ArticleData::FIELD_TITLE, 'test');

        $this->assertInternalType('array', $sortedArticles, 'Returned type should be set to array');
        $this->assertEquals([], $sortedArticles, 'Search filter should return an empty array when no article passed');
        $this->assertEquals(0, count($sortedArticles));
    }

    /**
     * Does sort method ordering elements in appropriate way
     */
    public function testIsSortReturnElementsWithBiggerValue()
    {
        $articleOne = new ApiArticle();
        $articleOne->setTitle('Test title');
        $articleTwo = new ApiArticle();
        $articleTwo->setTitle('Article Title');

        $articles = [$articleOne, $articleTwo];
        $oneArticleSearched = $this->searchFormFilter->filter($articles, ArticleData::FIELD_TITLE, 'test');
        $twoArticlesSearched = $this->searchFormFilter->filter($articles, ArticleData::FIELD_TITLE, 'title');

        $this->assertEquals(1, count($oneArticleSearched), 'Returned array should have only one element');
        $this->assertEquals(2, count($twoArticlesSearched), 'Returned array should have two elements');

        /** Check searched articles - is this correct article */
        $this->assertEquals('Test title', $oneArticleSearched[0]->getTitle(), 'Wrong article find');
    }
}
