<?php

namespace Tests\Unit\Service;

use Jamf\BlogBundle\Entity\ApiArticle;
use Jamf\BlogBundle\Service\ApiArticleGroup;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiArticleGroupTest extends WebTestCase
{
    /**
     * @var ApiArticleGroup
     */
    private $apiArticleGroup;

    public function setUp()
    {
        $this->apiArticleGroup = new ApiArticleGroup();
    }

    /**
     * Return type array or empty array
     */
    public function testIsGroupReturnArray()
    {
        $groupedArticles = $this->apiArticleGroup->group([]);
        $this->assertInternalType('array', $groupedArticles, 'Returned type should be set to array');
        $this->assertEquals(0, count($groupedArticles));
    }

    /**
     * Does filter method removing appropriate elements
     */
    public function testIsArticleAreGroupedCorrectly()
    {
        $articleOne = new ApiArticle();
        $articleOne->setDateCreated(1514764800); //1514764800 = 1 January 2018
        $articleTwo = new ApiArticle();
        $articleTwo->setDateCreated(1514764799); //1514764799 = 1 January 2018

        $articles = [$articleOne, $articleTwo];
        $groupedArticles = $this->apiArticleGroup->group($articles);

        /** Check is set apropriate array key - Month Year */
        $this->assertEquals(true, isset($groupedArticles['January 2018']), 'Expected array key "January 2018"');

        /** Check is grouping method collect two aray elemnts into one subarray with two elements */
        $this->assertEquals(2, count($groupedArticles['January 2018']), 'There should be two articles in this sub array');
    }
}
