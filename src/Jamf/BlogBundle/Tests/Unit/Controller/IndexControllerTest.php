<?php

namespace Tests\Unit\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class IndexControllerTest extends WebTestCase
{
    /**
     * @var Client A Client instance
     */
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
        $this->client->request('GET', '/');
        parent::setUp();
    }

    /**
     * Main page - check returning type object
     */
    public function testIsIndexHasBlogHeader()
    {
        $this->assertContains('The Blog', $this->client->getResponse()->getContent());
    }

    /**
     * Checking search form
     */
    public function testIsIndexHasSearchField()
    {
        $this->assertContains(
            'id="blog_search_form_term"',
            $this->client->getResponse()->getContent(),
            'Main page should have defined search form with text field id "blog_search_form_term"'
        );

        $this->assertContains(
            'form name="blog_search_form"',
            $this->client->getResponse()->getContent(),
            'Main page should have defined form with name "blog_search_form"'
        );

    }
}
