<?php

namespace Tests\Unit\Form;

use Jamf\BlogBundle\Form\SearchType;
use Symfony\Component\Form\Test\TypeTestCase;

class FormTest extends TypeTestCase
{
    /**
     * Is blox prefix returns specific string
     */
    public function testGetBloxPrefix()
    {
        $form = new SearchType();

        $this->assertEquals('blog_search_form', $form->getBlockPrefix());
        $this->assertInternalType('string', $form->getBlockPrefix(), 'Methods should return string type data');
    }

    /**
     * Form search test
     */
    public function testSubmitValidData()
    {
        $formData = [
            'test' => 'test',
            'test2' => 'test2',
        ];

        $objectToCompare = new SearchType();
        $form = $this->factory->create(SearchType::class, $objectToCompare);

        $object = new SearchType();
        $form->submit($formData);

        // check that $objectToCompare was modified as expected when the form was submitted
        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($object, $objectToCompare);

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
}
