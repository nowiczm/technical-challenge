<?php

namespace Tests\Unit\Dictionary;

use Jamf\BlogBundle\Dictionary\ArticleData;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ArticleDataTest extends WebTestCase
{
    /**
     * Testing constant values
     */
    public function testConstValues()
    {
        $this->assertEquals(ArticleData::FIELD_ID, 'id', 'Unexpected const value');
        $this->assertEquals(ArticleData::FIELD_ENTITY_ID, 'entityId', 'Unexpected const value');
        $this->assertEquals(ArticleData::FIELD_DATE_CREATED, 'dateCreated', 'Unexpected const value');
        $this->assertEquals(ArticleData::FIELD_TITLE, 'title', 'Unexpected const value');
    }
}
