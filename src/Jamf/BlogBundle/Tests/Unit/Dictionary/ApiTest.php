<?php

namespace Tests\Unit\Dictionary;

use Jamf\BlogBundle\Dictionary\Api;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiTest extends WebTestCase
{
    /**
     * Testing constant values
     */
    public function testConstValues()
    {
        $this->assertEquals(Api::GET_METHOD, 'GET', 'Unexpected const value');
        $this->assertEquals(Api::JAMF_API_PATH, 'https://www.jamf.com/api/entries/blog/', 'Unexpected const value');
    }
}
