<?php

namespace Tests\Unit\Facade;

use Doctrine\Common\Collections\Criteria;
use Jamf\BlogBundle\Dictionary\ArticleData;
use Jamf\BlogBundle\Facade\Finder;
use Jamf\BlogBundle\Service\ApiArticleFilter;
use Jamf\BlogBundle\Service\ApiArticleGroup;
use Jamf\BlogBundle\Service\ApiArticleSort;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FinderTest extends WebTestCase
{
    /**
     * @var Finder
     */
    private $finder;

    public function setUp()
    {
        $this->finder = new Finder(
            new ApiArticleGroup(),
            new ApiArticleSort(new Criteria()),
            new ApiArticleFilter()
        );
    }

    /**
     * Return type array or empty array
     */
    public function testIsFilterReturnArray()
    {
        $finder = $this->finder->searchAndGroup(
            [],
            ArticleData::FIELD_DATE_CREATED,
            Criteria::DESC,
            ArticleData::FIELD_DATE_CREATED,
            123
        );

        $this->assertInternalType('array', $finder, 'Returned type should be set to array');
        $this->assertEquals([], $finder, 'Finder should return an empty array when no article passed');
        $this->assertEquals(0, count($finder));
    }
}
