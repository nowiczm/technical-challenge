<?php

namespace Tests\Unit\Entity;

use Jamf\BlogBundle\Entity\ApiArticle;
use Jamf\BlogBundle\Model\AbstractArticle;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiArticleTest extends WebTestCase
{
    /**
     * @var ApiArticle
     */
    private $apiArticle;

    public function setUp()
    {
        parent::setUp();
        $this->apiArticle = new ApiArticle();
    }

    /**
     * Testing all setters
     */
    public function testSettersAndGetters()
    {
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setId(10));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setStatus('open'));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setCanonicalUrl('http://www.google.pl'));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setContent('content text'));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setCreatedById(2));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setDateCreated(1514764800));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setDateExpired(1514764801));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setDateModified(1514764802));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setEntityId(123));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setGated(true));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setHref('www.jamf.com'));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setLanguage('en'));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setMetaDescription('meta description test'));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setMetaTitle('Article title'));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setModifiedById(111));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setPrimaryCategory('blog'));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setSlug('blog-article'));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setSummary('summary text'));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setTags(['one', 'two']));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setThumbnailImage('image.jpg'));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setTitle('Title new article'));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setTranslatedPrimaryCategory(22));
        $this->assertInstanceOf(AbstractArticle::class, $this->apiArticle->setVideo('youtube.com'));

        $this->assertEquals(10, $this->apiArticle->getId());
        $this->assertEquals('open', $this->apiArticle->getStatus());
        $this->assertEquals('http://www.google.pl', $this->apiArticle->getCanonicalUrl());
        $this->assertEquals('content text', $this->apiArticle->getContent());
        $this->assertEquals(2, $this->apiArticle->getCreatedById());
        $this->assertEquals(1514764800, $this->apiArticle->getDateCreated());
        $this->assertEquals(1514764801, $this->apiArticle->getDateExpired());
        $this->assertEquals(1514764802, $this->apiArticle->getDateModified());
        $this->assertEquals(123, $this->apiArticle->getEntityId());
        $this->assertEquals(true, $this->apiArticle->getGated());
        $this->assertEquals('www.jamf.com', $this->apiArticle->getHref());
        $this->assertEquals('en', $this->apiArticle->getLanguage());
        $this->assertEquals('meta description test', $this->apiArticle->getMetaDescription());
        $this->assertEquals('Article title', $this->apiArticle->getMetaTitle());
        $this->assertEquals(111, $this->apiArticle->getModifiedById());
        $this->assertEquals('blog', $this->apiArticle->getPrimaryCategory());
        $this->assertEquals('blog-article', $this->apiArticle->getSlug());
        $this->assertEquals('summary text', $this->apiArticle->getSummary());
        $this->assertEquals(['one', 'two'], $this->apiArticle->getTags());
        $this->assertEquals('image.jpg', $this->apiArticle->getThumbnailImage());
        $this->assertEquals('Title new article', $this->apiArticle->getTitle());
        $this->assertEquals(22, $this->apiArticle->getTranslatedPrimaryCategory());
        $this->assertEquals('youtube.com', $this->apiArticle->getVideo());
    }
}
