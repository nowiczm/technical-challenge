<?php

namespace Tests\Unit\Repository;

use Jamf\BlogBundle\Exception\NoArticleException;
use Jamf\BlogBundle\Repository\ApiArticleRepository;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Serializer\SerializerInterface;

class ApiArticleRepositoryTest extends WebTestCase
{
    /**
     * @var ApiArticleRepository
     */
    private $apiArticleRepository;

    /**
     * Set up guzzle client and api article repository
     */
    public function setUp()
    {
        $loggerMock = $this->getMockBuilder(Logger::class)
            ->disableOriginalConstructor()
            ->getMock();

        $serializerMock = $this->getMockBuilder(SerializerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockResponse = new MockHandler([
            new Response(200, ['X-Foo' => 'Bar']),
            new Response(202, ['Content-Length' => 0]),
            new RequestException("Error Communicating with Server", new Request('GET', 'test'))
        ]);

        $handler = HandlerStack::create($mockResponse);
        $clientMock = new Client(['handler' => $handler]);

        $this->apiArticleRepository = new ApiArticleRepository($loggerMock, $clientMock, $serializerMock, 'www.google.pl');
        parent::setUp();
    }

    /**
     * Checks throw exception on no article
     */
    public function testThrowNoArticleException()
    {
        $this->expectException(NoArticleException::class);
        $this->assertInternalType('array', $this->apiArticleRepository->getList());
    }
}
