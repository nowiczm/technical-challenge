<?php

namespace Tests\Unit\Repository;

use Jamf\BlogBundle\Repository\ApiArticleRepository;
use Jamf\BlogBundle\Repository\DatabaseArticleRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DatabaseArticleRepositoryTest extends WebTestCase
{
    /**
     * @var ApiArticleRepository
     */
    private $databaseArticleRepository;

    /**
     * Set up database repository
     */
    public function setUp()
    {
        $this->databaseArticleRepository = new DatabaseArticleRepository();
        parent::setUp();
    }

    /**
     * Checks returning type of database repository
     */
    public function testThrowNoArticleException()
    {
        $this->assertInternalType('array', $this->databaseArticleRepository->getList());
    }
}
