<?php

namespace Jamf\BlogBundle\Service;

use Jamf\BlogBundle\Model\AbstractArticle;
use Jamf\BlogBundle\Model\GroupInterface;

/**
 * Grouping articles gets from API point
 */
class ApiArticleGroup implements GroupInterface
{
    /**
     * @param array $dataToGroup
     * @return array
     */
    public function group(array $dataToGroup): array
    {
        $groupedArticles = [];
        $date = new \DateTime();

        /** @var AbstractArticle $article */
        foreach ($dataToGroup as $article) {
            $date->setTimestamp($article->getDateCreated());
            $dateMonth = \DateTime::createFromFormat('!m', $date->format('m'));
            $groupedArticles[$dateMonth->format('F') . ' ' . $date->format('Y')][] = $article;
        }

        return $groupedArticles;
    }
}
