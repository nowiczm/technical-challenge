<?php

namespace Jamf\BlogBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Jamf\BlogBundle\Model\AbstractArticle;
use Jamf\BlogBundle\Model\FilterInterface;

/**
 * @todo Add mapper for $fieldValue and methods from object $article which be called, based on field name
 *
 * Filter articles by specific fileds
 */
class ApiArticleFilter implements FilterInterface
{
    /**
     * @param array $articles
     * @param string $filterByFieldName
     * @param string $fieldValue
     *
     * @return array
     */
    public function filter(array $articles, string $filterByFieldName, string $fieldValue): array
    {
        $articlesCollection = new ArrayCollection($articles);

        $articlesCollection = $articlesCollection->filter(function (AbstractArticle $article) use ($fieldValue) {
            return $article->getDateCreated() > $fieldValue;
        });

        return $articlesCollection->isEmpty() ? [] : $articlesCollection->toArray();
    }
}
