<?php

namespace Jamf\BlogBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Jamf\BlogBundle\Model\SortInterface;

/**
 * Sorting articles from API point with specific conditions
 */
class ApiArticleSort implements SortInterface
{
    /**
     * @var Criteria
     */
    private $criteria;

    /**
     * ApiArticleSort constructor.
     * @param Criteria $criteria
     */
    public function __construct(Criteria $criteria)
    {
        $this->criteria = $criteria;
    }

    /**
     * {@inheritdoc}
     */
    public function sort(array $articles, string $sortByFieldName, string $sortType = Criteria::ASC): array
    {
        $articlesCollection = new ArrayCollection($articles);

        $this->criteria->orderBy([$sortByFieldName => $sortType]);
        $articlesCollection = $articlesCollection->matching($this->criteria);

        return $articlesCollection->isEmpty() ? [] : $articlesCollection->toArray();
    }
}
