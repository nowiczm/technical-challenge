<?php

namespace Jamf\BlogBundle\Entity;

use Jamf\BlogBundle\Model\AbstractArticle;

/**
 * Article get from API point
 */
class ApiArticle extends AbstractArticle
{
}