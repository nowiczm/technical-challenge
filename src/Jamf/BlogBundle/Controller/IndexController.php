<?php

namespace Jamf\BlogBundle\Controller;

use Doctrine\Common\Collections\Criteria;
use Jamf\BlogBundle\Dictionary\ArticleData;
use Jamf\BlogBundle\Exception\NoArticleException;
use Jamf\BlogBundle\Facade\Finder;
use Jamf\BlogBundle\Form\SearchType;
use Jamf\BlogBundle\Model\FilterInterface;
use Jamf\BlogBundle\Repository\ArticleRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Main page with articles
 */
class IndexController extends Controller
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    /**
     * @var Finder
     */
    private $finder;

    /**
     * @var FilterInterface
     */
    private $searchFilter;

    /**
     * @param LoggerInterface $logger
     * @param ArticleRepositoryInterface $articleRepository
     * @param Finder $finder
     * @param FilterInterface $searchFilter
     */
    public function __construct(
        LoggerInterface $logger,
        ArticleRepositoryInterface $articleRepository,
        Finder $finder,
        FilterInterface $searchFilter
    ) {
        $this->logger = $logger;
        $this->articleRepository = $articleRepository;
        $this->finder = $finder;
        $this->searchFilter = $searchFilter;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $searchedArticles = [];
        $articles = [];

        try {
            /** Gets list of articles */
            $articles = $this->articleRepository->getList();

            /** Filter, sort and group articles with custom conditions, 1514764800 = 1 Jun 2018 */
            $searchedArticles = $this->finder->searchAndGroup(
                $articles,
                ArticleData::FIELD_DATE_CREATED,
                Criteria::DESC,
                ArticleData::FIELD_DATE_CREATED,
                1514764800
            );
        } catch (NoArticleException $e) {
            $this->logger->error($e->getMessage());
        }

        /** Create search form */
        $searchForm = $this->createForm(SearchType::class);
        $searchForm->handleRequest($request);
        $formValues = $this->getSearchFormValue($searchForm, $articles);

        $template = '@JamfBlog/Index/index.html.twig';
        /** Load other template HTML */
        if ($request->get('template') !== null) {
            $template = '@JamfBlog/UnofficialTemplate/index.html.twig';
        }

        return $this->render($template, [
            'articles' => $searchedArticles,
            'search_form' => $searchForm->createView(),
            'search_form_articles' => $formValues['search_form_articles'],
            'search_phrase' => $formValues['search_phrase'],
            'is_submitted' => $searchForm->isSubmitted()
        ]);
    }

    /**
     * @param FormInterface $searchForm
     * @param array $articles
     *
     * @return array
     */
    private function getSearchFormValue(FormInterface $searchForm, array $articles): array
    {
        $searchFormArticles = [];
        $searchedValue = null;
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $searchedValue = $searchForm->getData()[SearchType::SEARCH_FIELD_NAME] ?? '';
            $searchFormArticles = $this->searchFilter->filter($articles, ArticleData::FIELD_TITLE, $searchedValue);
        }

        return [
            'search_form_articles' => $searchFormArticles,
            'search_phrase' => $searchedValue
        ];
    }
}
