<?php

namespace Jamf\BlogBundle\Dictionary;

/**
 * Const for API
 */
class Api
{
    /**
     * API point with blog articles
     */
    public const JAMF_API_PATH = 'https://www.jamf.com/api/entries/blog/';

    /**
     * Guzzle get request method name
     */
    public const GET_METHOD = 'GET';
}