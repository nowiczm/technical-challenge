<?php

namespace Jamf\BlogBundle\Dictionary;

/**
 * Const with the fields assigned to article
 */
class ArticleData
{
    public const FIELD_ID = 'id';
    public const FIELD_ENTITY_ID = 'entityId';
    public const FIELD_DATE_CREATED = 'dateCreated';
    public const FIELD_TITLE = 'title';
}